package com.baccredomatic.baccontactsmicroservice.controllers;

import com.baccredomatic.baccontactsmicroservice.domain.Contact;
import com.baccredomatic.baccontactsmicroservice.services.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class ContactsController {

    private static final Logger log = LoggerFactory.getLogger(ContactsController.class);

    @Autowired
    ContactService contactService;

    @GetMapping("/profile/{userId}")
    public ResponseEntity<Object> getUserContactData(@PathVariable("userId") String userId) {
        Contact contact = contactService.getContact(userId);
        log.info("Contact "+userId+" Found");
        return new ResponseEntity<>(contact, HttpStatus.OK);
    }

    @PostMapping(value = "/profile", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> insertUserContactData(@RequestBody Contact contact) {
        contactService.saveContact(contact);
        return new ResponseEntity<>(contact, HttpStatus.OK);
    }

    @PutMapping(value = "/profile/{userId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateUserContactData(@RequestBody Contact contact) {
        contactService.updateContact(contact);
        return new ResponseEntity<>(contact, HttpStatus.OK);
    }

    @PutMapping("/profile/{userId}/directory/{contactId}")
    public ResponseEntity<Object> addToUserDirectory(@PathVariable("userId") String userId, @PathVariable("contactId") String contactId) {
        contactService.addContactToDirectory(userId, contactId);
        return new ResponseEntity<>(contactId, HttpStatus.OK);
    }

    @DeleteMapping("/profile/{userId}/directory/{contactId}")
    public ResponseEntity<Object> removeToUserDirectory(@PathVariable("userId") String userId, @PathVariable("contactId") String contactId) {
        contactService.removeContactToDirectory(userId, contactId);
        return new ResponseEntity<>(contactId, HttpStatus.OK);
    }

    @PutMapping("/profile/{userId}/directory-extended/{contactId}")
    public ResponseEntity<Object> addToUserDirectoryExtended(@PathVariable("userId") String userId, @PathVariable("contactId") String contactId, @RequestParam(name = "isFavorite", defaultValue = "false") String isFavoriteParam) {
        contactService.addContactToDirectoryExtended(userId, contactId, Boolean.parseBoolean(isFavoriteParam.toLowerCase()));
        return new ResponseEntity<>(contactId, HttpStatus.OK);
    }

    @DeleteMapping("/profile/{userId}/directory-extended/{contactId}")
    public ResponseEntity<Object> removeToUserDirectoryExtended(@PathVariable("userId") String userId, @PathVariable("contactId") String contactId) {
        contactService.removeContactToDirectoryExtended(userId, contactId);
        return new ResponseEntity<>(contactId, HttpStatus.OK);
    }

    @GetMapping(value = "/profile/{userId}/favorites-directory", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<String> removeToUserDirectoryExtended(@PathVariable("userId") String userId) {
        return contactService.getAllFavorites(userId);
    }
}
