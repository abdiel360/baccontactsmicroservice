package com.baccredomatic.baccontactsmicroservice.services;

import com.baccredomatic.baccontactsmicroservice.dao.ContactDirectoryRepository;
import com.baccredomatic.baccontactsmicroservice.dao.ContactRepository;
import com.baccredomatic.baccontactsmicroservice.domain.Contact;
import com.baccredomatic.baccontactsmicroservice.domain.ContactDirectoryEntry;
import com.baccredomatic.baccontactsmicroservice.domain.ContactDirectoryEntryKey;
import com.baccredomatic.baccontactsmicroservice.domain.ContactPreview;
import com.baccredomatic.baccontactsmicroservice.exceptions.MyContactException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ContactService {

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ContactDirectoryRepository contactDirectoryRepository;

    public Contact getContact(String userId){
        Contact contact = contactRepository.findByUserId(userId);

        if (contact == null) {
            throw new MyContactException("contact not found", HttpStatus.NOT_FOUND, "002");
        }

        return contact;
    }

    public void saveContact(Contact newContact) {
        newContact.setPublicUserId(UUID.randomUUID().toString());

        contactRepository.saveNew(newContact);
    }

    public void updateContact(Contact newContact) {
        if (contactRepository.existsById(newContact.getUserId())) {
            contactRepository.save(newContact);
        } else {
            throw new MyContactException("contact not found", HttpStatus.NOT_FOUND, "002");
        }
    }

    public void addContactToDirectory(String userId, String contactId) {
        Contact userProfile = contactRepository.findByUserId(userId);
        userProfile.getContactList().add(new ContactPreview(contactId));

        contactRepository.save(userProfile);
    }

    public void removeContactToDirectory(String userId, String contactId) {
        Contact userProfile = contactRepository.findByUserId(userId);
        userProfile.getContactList().removeIf(contactEntry -> contactEntry.getUserId().equalsIgnoreCase(contactId));

        contactRepository.save(userProfile);
    }

    public void addContactToDirectoryExtended(String userId, String contactId, boolean isFavorite) {
        ContactDirectoryEntry directoryEntry = new ContactDirectoryEntry(userId, contactId, isFavorite);

        contactDirectoryRepository.save(directoryEntry);
    }

    public void removeContactToDirectoryExtended(String userId, String contactId) {
        contactDirectoryRepository.deleteById(new ContactDirectoryEntryKey(userId, contactId));
    }

    public Set<String> getAllFavorites(String userId) {
        return contactDirectoryRepository.getAllFromUserId(userId).stream().map(entry -> entry.getContactDirectoryEntryKey().getTargetUserId()).collect(Collectors.toSet());
    }
}
