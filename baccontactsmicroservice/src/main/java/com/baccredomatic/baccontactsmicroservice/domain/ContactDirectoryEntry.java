package com.baccredomatic.baccontactsmicroservice.domain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "USER_CONTACT_BAC")
public class ContactDirectoryEntry {
    /*
    En caso de entidades con llaves compuestas se puede hacer uso de un POJO separado para el mapeo de la identificacion
    de la entidad
     */
    private ContactDirectoryEntryKey contactDirectoryEntryKey;
    private Boolean favorite;
    private LocalDateTime creationDate;
    private LocalDateTime modificationDate;

    public ContactDirectoryEntry() {
    }

    public ContactDirectoryEntry(String userId, String targetUserId, Boolean favorite) {
        this.contactDirectoryEntryKey = new ContactDirectoryEntryKey(userId, targetUserId);
        this.favorite = favorite;
    }

    @EmbeddedId
    public ContactDirectoryEntryKey getContactDirectoryEntryKey() {
        return contactDirectoryEntryKey;
    }

    public void setContactDirectoryEntryKey(ContactDirectoryEntryKey contactDirectoryEntryKey) {
        this.contactDirectoryEntryKey = contactDirectoryEntryKey;
    }

    @Column(name = "IS_FAVORITE")
    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    @Column(name = "CREATION_DATE", updatable = false)
    @CreationTimestamp
    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "MODIFICATION_DATE")
    @UpdateTimestamp
    public LocalDateTime getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }
}
