package com.baccredomatic.baccontactsmicroservice.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/*
Esta clase es para el mapeo de la llave compuesta de una entidad, es necesario la etiqueta Embeddable e implementar la
clase Serializable
 */
@Embeddable
public class ContactDirectoryEntryKey implements Serializable {
    private String userId;
    private String targetUserId;

    public ContactDirectoryEntryKey() {
    }

    public ContactDirectoryEntryKey(String userId, String targetUserId) {
        this.userId = userId;
        this.targetUserId = targetUserId;
    }

    @Column(name = "ID_CONTACT_USER")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "ID_CONTACT_TARGET")
    public String getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(String targetUserId) {
        this.targetUserId = targetUserId;
    }
}
