package com.baccredomatic.baccontactsmicroservice.domain;

import javax.persistence.*;

/*
Tambien se puede definir entidades de forma parcial, donde se obtenga columnas especificas
 */
@Entity
@Table(name = "CONTACTO_BAC")
public class ContactPreview {

    private String userId;
    private String publicUserId;
    private String phoneNumber;

    protected ContactPreview() {
        //for jpa
    }

    public ContactPreview(String userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "USER_ID")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "PUBLIC_USER_ID", insertable = false, updatable = false)
    public String getPublicUserId() {
        return publicUserId;
    }

    public void setPublicUserId(String publicUserId) {
        this.publicUserId = publicUserId;
    }

    @Column(name = "PHONE_NUMBER", insertable = false, updatable = false)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
