package com.baccredomatic.baccontactsmicroservice.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
/*
Se puede definir el nombre de la tabla en DB, asi como constraints unicos para casos como el del id publico
 */
@Table(name = "CONTACTO_BAC", uniqueConstraints = {
        @UniqueConstraint(columnNames = "PUBLIC_USER_ID", name = "uniquePublicUserIdConstraint"),
        @UniqueConstraint(columnNames = "PHONE_NUMBER", name = "uniquePhoneNumberConstraint")
})
public class Contact {

    private String userId;
    private String publicUserId;
    private String phoneNumber;
    private String localAccount;
    private String dollarAccount;
    private Set<ContactPreview> contactList = new HashSet<>();

    @Id
    @Column(name = "USER_ID")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "PUBLIC_USER_ID")
    public String getPublicUserId() {
        return publicUserId;
    }

    public void setPublicUserId(String publicUserId) {
        this.publicUserId = publicUserId;
    }

    @Column(name = "PHONE_NUMBER", unique = true, updatable = false)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column(name = "LOCAL_ACCOUNT")
    public String getLocalAccount() {
        return localAccount;
    }

    public void setLocalAccount(String localAccount) {
        this.localAccount = localAccount;
    }

    @Column(name = "DOLLAR_ACCOUNT")
    public String getDollarAccount() {
        return dollarAccount;
    }

    public void setDollarAccount(String dollarAccount) {
        this.dollarAccount = dollarAccount;
    }

    /*
    Se puede mapear uniones de tablas con relaciones de ManyToMany entre otros sin la necesidad de mapear la entidad
    intermedia

    Tambien se puede definir el mapeo de las tablas, asi como sus columnas
     */
    @ManyToMany()
    @JoinTable(name = "USER_CONTACT_BAC", joinColumns = @JoinColumn(name = "ID_CONTACT_USER"), inverseJoinColumns = @JoinColumn(name = "ID_CONTACT_TARGET"))
    public Set<ContactPreview> getContactList() {
        return contactList;
    }

    public void setContactList(Set<ContactPreview> contactList) {
        this.contactList = contactList;
    }
}
