package com.baccredomatic.baccontactsmicroservice.dao;

import com.baccredomatic.baccontactsmicroservice.domain.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/*
Repositorio que define la interfaz de los metodos de acceso por JPA a DB, esta especificado hacia la entidad
Contact con una llave del tipo String
 */
@Repository
public interface ContactRepository extends JpaRepository<Contact, String> {

    /*
    Se puede definir metodos de busqueda por columna sin la necesidad de definicion de Query

    Esto es derivado por el nombre del metodo
    https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.sample-app.finders.strategies
     */
    Contact findByUserId(String userId);

    /*
    Usualmente los Query son para el uso de lectura de registros, pero se puede definir tambien para escritura agregando
    las anotaciones de Transactional y Modifying

    Para acceder a atributos de un objecto pasado por parametro al Query se puede acceder con :#{#parametro.atributo}
     */
    @Transactional
    @Modifying
    @Query(value = "insert into CONTACTO_BAC (USER_ID, PUBLIC_USER_ID, PHONE_NUMBER, LOCAL_ACCOUNT, DOLLAR_ACCOUNT) values (:#{#c.userId}, :#{#c.publicUserId}, :#{#c.phoneNumber}, :#{#c.localAccount}, :#{#c.dollarAccount})", nativeQuery = true)
    void saveNew(@Param("c") Contact contact);
}
