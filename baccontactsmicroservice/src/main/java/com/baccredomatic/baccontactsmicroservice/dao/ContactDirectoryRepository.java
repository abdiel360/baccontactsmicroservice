package com.baccredomatic.baccontactsmicroservice.dao;

import com.baccredomatic.baccontactsmicroservice.domain.Contact;
import com.baccredomatic.baccontactsmicroservice.domain.ContactDirectoryEntry;
import com.baccredomatic.baccontactsmicroservice.domain.ContactDirectoryEntryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
Repositorio que define la interfaz de los metodos de acceso por JPA a DB, esta especificado hacia la entidad
ContactDirectoryEntry con la llave compuesta definida en ContactDirectoryEntryKey
 */
@Repository
public interface ContactDirectoryRepository extends JpaRepository<ContactDirectoryEntry, ContactDirectoryEntryKey> {

    /*
    Se puede definir un Query personalizado donde podemos aplicar filtros adicionales y la aplicacion de orden de
    registros

    No es un query nativo, por lo que las referencias de Tablas y Columnas son a como estan definidos en el POJO de la
    Entidad
     */
    @Query(value = "SELECT c FROM ContactDirectoryEntry c WHERE c.contactDirectoryEntryKey.userId = :userId AND c.favorite = true")
    List<ContactDirectoryEntry> getAllFromUserId(@Param("userId") String userId);
}
