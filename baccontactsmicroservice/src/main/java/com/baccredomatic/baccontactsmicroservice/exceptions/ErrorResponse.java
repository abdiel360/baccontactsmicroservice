package com.baccredomatic.baccontactsmicroservice.exceptions;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

/*
Esta clase es solo para definir el mapeo del JSON usado para la respuesta del ExceptionHandlingController
 */
public class ErrorResponse {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private final LocalDateTime timestamp;
    private final HttpStatus status;
    private final String message;
    private final String code;

    public ErrorResponse(LocalDateTime timestamp, HttpStatus status, String message, String code) {
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.code = code;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }
}
