package com.baccredomatic.baccontactsmicroservice.exceptions;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

/*
Este controlador sirve para el mapeo de excepciones de salida al microservicio, tanto el codigo HTTP como la respuesta
 */
@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

    /*
    Se puede capturar toda excepcion no controlada...
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAnyException(Exception e) {
        ErrorResponse err = new ErrorResponse(
                LocalDateTime.now(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                e.getMessage(),
                "000"
        );

        return new ResponseEntity<>(err, err.getStatus());
    }

    /*
    ...asi como excepciones especificas...
     */
    @ExceptionHandler({ConstraintViolationException.class, DataIntegrityViolationException.class})
    public ResponseEntity<?> handleConstraintViolationException(Exception e) {
        ErrorResponse err = new ErrorResponse(
                LocalDateTime.now(),
                HttpStatus.BAD_REQUEST,
                e.getMessage(),
                "001"
        );

        return new ResponseEntity<>(err, err.getStatus());
    }

    /*
    ...y excepciones creadas en el proyecto
     */
    @ExceptionHandler(MyContactException.class)
    public ResponseEntity<Object> handleMyContactException(MyContactException e) {
        ErrorResponse err = new ErrorResponse(
                LocalDateTime.now(),
                e.getHttpStatus(),
                e.getMessage(),
                e.getCode()
        );

        return new ResponseEntity<>(err, err.getStatus());
    }
}
