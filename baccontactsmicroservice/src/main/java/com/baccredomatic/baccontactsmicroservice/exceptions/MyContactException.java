package com.baccredomatic.baccontactsmicroservice.exceptions;

import org.springframework.http.HttpStatus;

/*
Esta excepcion permite la definicion de atributos adicionales como el codigo http y atributos de la respuesta generada
para el ExceptionHandlingController
 */
public class MyContactException extends RuntimeException {
    private final HttpStatus httpStatus;
    private final String code;

    public MyContactException(String message, Throwable cause, HttpStatus httpStatus, String code) {
        super(message, cause);
        this.httpStatus = httpStatus;
        this.code = code;
    }

    public MyContactException(String message, HttpStatus httpStatus, String code) {
        super(message);
        this.httpStatus = httpStatus;
        this.code = code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getCode() {
        return code;
    }
}
