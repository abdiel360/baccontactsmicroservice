package com.baccredomatic.baccontactsmicroservice;

import com.baccredomatic.baccontactsmicroservice.dao.ContactRepository;
import com.baccredomatic.baccontactsmicroservice.domain.Contact;
import com.baccredomatic.baccontactsmicroservice.domain.ContactPreview;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.UUID;

@SpringBootApplication
public class BaccontactsmicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaccontactsmicroserviceApplication.class, args);
	}

	@Bean
	public CommandLineRunner initDB(ContactRepository contactRepository) {
		return (args -> {
			Contact contactSteven = new Contact();
			contactSteven.setUserId("stevenvb27");
			contactSteven.setPublicUserId(UUID.randomUUID().toString());
			contactSteven.setPhoneNumber("50672716687");
			contactSteven.setLocalAccount("95876321");
			contactSteven.setDollarAccount("96874235");

			Contact contactAbdiel = new Contact();
			contactAbdiel.setUserId("abdiel9876");
			contactAbdiel.setPublicUserId(UUID.randomUUID().toString());
			contactAbdiel.setPhoneNumber("50687526314");
			contactAbdiel.setLocalAccount("123456789");
			contactAbdiel.setDollarAccount("987654321");

			Contact contactChava = new Contact();
			contactChava.setUserId("elchava56");
			contactChava.setPublicUserId(UUID.randomUUID().toString());
			contactChava.setPhoneNumber("50652109874");
			contactChava.setLocalAccount("321456987");
			contactChava.setDollarAccount("789654123");

			contactRepository.save(contactSteven);
			contactRepository.save(contactChava);
			contactRepository.save(contactAbdiel);

			contactSteven.getContactList().add(new ContactPreview("abdiel9876"));
			contactSteven.getContactList().add(new ContactPreview("elchava56"));
			contactChava.getContactList().add(new ContactPreview("abdiel9876"));

			contactRepository.save(contactSteven);
			contactRepository.save(contactChava);
		});
	}
}
