package com.baccredomatic.baccontactsmicroservice.controllers;

import com.baccredomatic.baccontactsmicroservice.dao.ContactRepository;
import com.baccredomatic.baccontactsmicroservice.domain.Contact;
import com.baccredomatic.baccontactsmicroservice.services.ContactService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ContactsController.class)
class ContactsControllerIntegrationTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private ContactService contactService;
    @MockBean
    private ContactRepository contactRepository;
    @BeforeEach
    void setUp() {
    }

    @Test
    void getContactStatusOk() throws Exception {
        Contact c = new Contact();
        c.setPhoneNumber("60482646");
        when(contactService.getContact("1")).thenReturn(c);

        mvc.perform(MockMvcRequestBuilders.get("/profile/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.phoneNumber").value("60482646"));
    }
    @Test
    void createContactStatusOk() throws Exception {
        Contact c = new Contact();
        c.setUserId("abdiel32");
        c.setPublicUserId("abdiel32");
        c.setPhoneNumber("24289086");
        mvc.perform(MockMvcRequestBuilders.post("/profile")
                .content(asJsonString(c))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}