package com.baccredomatic.baccontactsmicroservice.controllers;

import com.baccredomatic.baccontactsmicroservice.domain.Contact;
import com.baccredomatic.baccontactsmicroservice.services.ContactService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContactsControllerTest {

    @InjectMocks
    ContactsController contactsController;
    @Mock
    private ContactService contactService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getUserContactData() {
        Contact c = new Contact();
        c.setPhoneNumber("60482646");
        when(contactService.getContact("1")).thenReturn(c);
        Contact contact=(Contact)contactsController.getUserContactData("1").getBody();
        assertNotNull(contact);
    }

    @Test
    void insertUserContactData() {
        Contact c = new Contact();
        c.setPhoneNumber("60482646");

        Contact contact=(Contact)contactsController.insertUserContactData(c).getBody();
        assertNotNull(contact);
    }

}