package com.baccredomatic.baccontactsmicroservice.services;

import com.baccredomatic.baccontactsmicroservice.dao.ContactDirectoryRepository;
import com.baccredomatic.baccontactsmicroservice.dao.ContactRepository;
import com.baccredomatic.baccontactsmicroservice.domain.Contact;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
class ContactServiceTest {
    @InjectMocks
    ContactService contactService;
    @Mock
    private ContactRepository contactRepository;
    @Mock
    private ContactDirectoryRepository contactDirectoryRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getContact() {
        Contact c = new Contact();
        c.setPhoneNumber("60482646");
        when(contactRepository.findByUserId("1")).thenReturn(c);

        Contact contact=contactService.getContact("1");
        assertNotNull(contact);

    }

}